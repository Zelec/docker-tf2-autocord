#!/usr/bin/dumb-init /bin/bash
echo "Starting Docker-TF2-Autocord..."

# Ripped from Linuxserver.io baseimages.
PUID=${PUID:-911}
PGID=${PGID:-911}

groupmod -o -g "$PGID" user
usermod -o -u "$PUID" user
chown -R user:user /app
chown -R user:user /config
chown -R user:user /defaults

# Checks if config already exists, if
if [ "$(ls -A '/config')" ]; then
    echo "Volume has files, using existing config..."
else
    echo "Volume Empty, copying base config files into volume..."
    cp -r /defaults/Login_details/* /config/
    chown -R user:user /config/*
    echo "...Done"
fi

find /config -type d -exec chmod 700 {} \;
find /config -type f -exec chmod 600 {} \;

su -l -s /bin/bash -c "cd /app && python3 /app/main.py" user