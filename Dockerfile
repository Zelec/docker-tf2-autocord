# Docker-TF2-Autocord, by Isaac Towns <Zelec@timeguard.ca>

# Rather than making the git repo a submodule of this repo, it would be better off to change the docker file as needed.
FROM alpine/git:1.0.7 AS repo
# Change target release tag here

# Normally alpine/git uses /git for it's workdir, however it's declared as a volume in the upstream resource
# so it has a hard time keeping data intact inbetween layers.
RUN mkdir /app
WORKDIR /app
RUN git clone https://github.com/Gobot1234/tf2-autocord.git .
    # && \
    # git checkout tags/${RELEASE}

# for Python builds, in my testing Alpine has more issues and is slower to 
# build compared to the Debian images for Python.
FROM library/python:3.8.1-buster AS docker-tf2-autocord
LABEL maintainer Isaac Towns <Zelec@timeguard.ca>
# Pull repo data from git container
COPY --from=repo /app/ /app
WORKDIR /app
# Installs updates, Moves config outside of /app, & setup unpriviledged user 
# (Adapted from Linuxserver.io base containers)
VOLUME ["/home/user", "/bot-files"]
RUN \
    rm -rf ./.git && \
    apt-get update && apt-get upgrade -y && apt-get install dumb-init && \
    mkdir -p /defaults /config /home/user && \
    mv -v /app/Login_details /defaults/Login_details && \
    ln -s /config /app/Login_details && \
    ln -s /config/tf2autocord.log /app/tf2autocord.log && \
    useradd -u 911 -U -d /home/user -s /bin/false user && \
    usermod -G users user && \
    rm -rf /tmp/* /var/tmp/* /var/lib/apt/lists/*
# Dependency install
RUN \
    pip install -U -r requirements.txt && \
    rm -rf /config/*
VOLUME ["/config"]
COPY entrypoint.sh /
ENTRYPOINT [ "/usr/bin/dumb-init" ]
CMD ["/entrypoint.sh"]