# Docker-TF2-Autocord
#### Maintained By: Isaac Towns <Zelec@timeguard.ca>
This repository is for my unofficial [Docker Image](https://hub.docker.com/r/zelec/tf2-autocord) for [tf2-autocord](https://github.com/Gobot1234/tf2-autocord) made by [Gobot1234](https://github.com/Gobot1234)

[tf2-autocord](https://github.com/Gobot1234/tf2-autocord) is a discord bot that works in tandem with [tf2-automatic](https://github.com/Nicklason/tf2-automatic), it allows you to manage and work with your bot through Discord instead of Steam Chat


## Quickstart
1. Make a docker-compose.yml file
```yaml
version: '2'
services:
  tf2-autocord:
    image: zelec/tf2-autocord:latest
    environment:
      - PUID=1000
      - PGID=1000
    restart: unless-stopped
    volumes: 
      - ./config:/config
      - ./Link to tf2-automatic's file folder:/bot-files
```
2. Replace the PUID and PGID values with your user's ids
  * If you don't know your PUID & PGID values are, type `echo -n "PUID:" && id -u "$USER" && echo -n "PGID:" && id -g "$USER"` into a terminal
3. Fix "Link to tf2-automatic's file folder" under volumes 
  * (If your tf2-automatic config lives under /home/user/tf2-automatic and if your bot's steam username is bot-01 for example, you need to point it to /home/user/tf2-automatic/files/bot-01)
4. Run `docker-compose up`
5. It should error, but now you will have items to edit in `config`, press Ctrl+C to stop docker-compose
6. Edit the files under `config` and follow along with the [tf2-autocord installation guide](https://github.com/Gobot1234/tf2-autocord/wiki/Installation)
7. One final `docker-compose up` will get the bot running